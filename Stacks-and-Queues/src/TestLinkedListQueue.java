
public class TestLinkedListQueue {

    public static void main(String[] args) {
        // Testing internal linkedlist queue.
        QueueUsingLinkedList<Integer> integerQueue = new QueueUsingLinkedList<Integer>();
        for (int i = 1; i <= 20; i++) {
            integerQueue.add(i);
            System.out.println("Inserted: " + i);
        }
        while (integerQueue.getSize() > 0)
            System.out.println("Removed: "
                    + Integer.toString(integerQueue.remove()));
        integerQueue.remove();
    }
}
