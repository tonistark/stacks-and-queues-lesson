
public class TestStack {

    public static void main(String[] args) {
        StackWithLinkedList<Integer> stack = new StackWithLinkedList<Integer>();
        for (int i = 1; i <= 10; i++)
            stack.offer(i);
        for (int i = 1; i <= 20; i++) {
            try {
                System.out.println("Next: " + Integer.toString(stack.peek()));
                System.out
                        .println("Removed: " + Integer.toString(stack.poll()));
            } catch (Exception e) {
                System.out.println("Error. Stack is empty.");
            }
        }
        for (int i = 1; i <= 10; i++)
            stack.add(i);
        for (int i = 1; i <= 20; i++) {
            try {
                System.out.println("Next: " + Integer.toString(stack.peek()));
                System.out.println("Removed: "
                        + Integer.toString(stack.remove()));
            } catch (Exception e) {
                System.out.println("Error. Stack is empty.");
            }
        }
    }
}
