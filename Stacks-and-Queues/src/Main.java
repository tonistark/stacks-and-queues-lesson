import java.util.LinkedList;

public class Main {

    public static void main(String[] args) {
        System.out.println("Please run each of the 3 testing classes,"
                + " TestLinkedListQueue, TestStack, and TestStandardQueue,"
                + " to test them.");
    }
}















/** A queue using a regular String[] array. */
class StringQueueRegularArray {

    public static final int DEFAULT_LIMIT = 100;
    private int limit, currentSize;
    private String[] queue;



    /**
     * Constructs the queue using the passed in initial size limit. Checks for a
     * valid value. If invalid, set the default size.
     */
    public StringQueueRegularArray(int inputLimit) {
        if (inputLimit < 0)
            limit = DEFAULT_LIMIT;
        else
            limit = inputLimit;
        queue = new String[limit];
        currentSize = 0;
    }



    /** Adds a String into the queue. Throws an exception if the queue is full. */
    public void add(String in) throws DataStructureFullException {
        if (currentSize >= limit)
            throw new DataStructureFullException();
        queue[currentSize] = in;
        currentSize++;
    }



    /** Adds a String into the queue. Returns false if the queue is full. */
    public boolean offer(String in) {
        if (currentSize >= limit)
            return false;
        queue[currentSize] = in;
        currentSize++;
        return true;
    }



    /**
     * Removes and returns the String from the front of the queue. Decrements
     * the current size. Throws an exception if the queue is empty.
     */
    public String remove() throws DataStructureEmptyException {
        if (currentSize == 0)
            throw new DataStructureEmptyException();
        // Hold this value for later return.
        String toReturn = queue[0];
        // We start counting from index 0. If the queue has size 3, then there
        // are elements in indices 0, 1, and 2. We set queue[0] to the value at
        // queue[1]. After queue[1] = queue[2], we stop.
        for (int i = 0; i < currentSize - 1; i++)
            queue[i] = queue[i + 1];
        // Decrement the size every time.
        currentSize--;
        return toReturn;
    }



    /**
     * Removes and returns the String from the front of the queue. Decrements
     * the current size. Returns null if the queue is empty.
     */
    public String poll() {
        if (currentSize == 0)
            return null;
        // Hold this value for later return.
        String toReturn = queue[0];
        // We start counting from index 0. If the queue has size 3, then there
        // are elements in indices 0, 1, and 2. We set queue[0] to the value at
        // queue[1]. After queue[1] = queue[2], we stop.
        for (int i = 0; i < currentSize - 1; i++)
            queue[i] = queue[i + 1];
        // Decrement the size every time.
        currentSize--;
        return toReturn;
    }



    /**
     * Returns the String at the front of the queue without removing it. Throws
     * an exception if the queue is empty.
     */
    public String peek() throws DataStructureEmptyException {
        if (currentSize == 0)
            throw new DataStructureEmptyException();
        return queue[0];
    }



    /** Returns the current size of the queue. */
    public int getSize() {
        return currentSize;
    }
}















/** A queue utilzing a LinkedList. */
class QueueUsingLinkedList<X> {

    private LinkedList<X> list;



    /** Constructs a MyQueue object that internally is a LinkedList. */
    public QueueUsingLinkedList() {
        list = new LinkedList<X>();
    }



    /** Adds an element to the end of the queue. */
    public void add(X element) {
        list.addLast(element);
    }



    /**
     * Removes and returns an element from the front of the queue. Throws an
     * exception if the queue is empty.
     */
    public X remove() {
        return list.removeFirst();
    }



    /** Returns the size of the queue. */
    public int getSize() {
        return list.size();
    }
}















/** A stack utilizing a LinkedList. The stack has no size limit. */
class StackWithLinkedList<X> {

    private LinkedList<X> list;



    /** Constructor. */
    public StackWithLinkedList() {
        list = new LinkedList<X>();
    }



    /** Adds to the stack. */
    public void add(X e) {
        list.addLast(e);
    }



    /**
     * Adds to the stack if it can (it should always be able to, since the stack
     * has no size limit.
     */
    public boolean offer(X e) {
        return list.add(e);
    }



    /**
     * Removes and returns the next item from the stack. Throws an exception if
     * the stack is empty.
     */
    public X remove() {
        return list.removeLast();
    }



    /**
     * Removes and returns the next item from the stack. Returns null if the
     * stack is empty.
     */
    public X poll() {
        return list.pollLast();
    }



    /** Returns the next item without removing it from the stack. */
    public X peek() {
        return list.getLast();
    }
}















class DataStructureFullException extends Exception {

    public DataStructureFullException() {
        super("The data structure is full! Cannot insert!");
    }
}















class DataStructureEmptyException extends Exception {

    public DataStructureEmptyException() {
        super("The data structure is empty! Cannot remove!");
    }
}