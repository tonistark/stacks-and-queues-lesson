public class TestStandardQueue {

    public static void main(String[] args) {
        // Testing poll, offer, and peek.
        StringQueueRegularArray queue = new StringQueueRegularArray(10);
        for (int i = 1; i < 20; i++) {
            if (queue.offer(Integer.toString(i)))
                System.out.println("Inserted: " + i);
            else
                System.out.println("Failed to insert: " + i);
            System.out.println("Current size: " + queue.getSize() + "\n");
        }
        for (int i = 1; i < 20; i++) {
            String nextValue = null;
            try {
                nextValue = queue.peek();
            } catch (DataStructureEmptyException e) {
                System.out.println("Failed to peek. Empty queue.");
            }
            System.out.println("Next value: " + nextValue);
            System.out.println("Removed: " + queue.poll());
            System.out.println("Current size: " + queue.getSize() + "\n");
        }

        // Testing add and remove.
        System.out.println("\n\n\nTesting part 2, add and remove!\n\n\n");
        StringQueueRegularArray queue2 = new StringQueueRegularArray(10);
        for (int i = 1; i < 20; i++) {
            try {
                queue2.add(Integer.toString(i));
            } catch (DataStructureFullException e) {
                System.out.println("Failed to insert");
            }
            System.out.println("Current size: " + queue2.getSize() + "\n");
        }
        for (int i = 1; i < 20; i++) {
            String nextValue = null;
            try {
                nextValue = queue2.peek();
            } catch (DataStructureEmptyException e) {
                System.out.println("Failed to peek. Empty queue2.");
            }
            System.out.println("Next value: " + nextValue);
            try {
                System.out.println("Removed: " + queue2.remove());
            } catch (DataStructureEmptyException e) {
                System.out.println("Failed to remove. Empty queue2.");
            }
            System.out.println("Current size: " + queue2.getSize() + "\n");
        }
    }
}
